package io.fajarca.project.extensions

import io.fajarca.project.config.constant.MovieConstant
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Locale
import java.util.TimeZone

fun String.toLocalDate(inFormat: String, outFormat: String): String {
    return try {
        val inputFormat = SimpleDateFormat(inFormat, Locale.US)
        inputFormat.timeZone = TimeZone.getTimeZone("GMT")

        val outputFormat = SimpleDateFormat(outFormat, Locale.getDefault())
        val date = inputFormat.parse(this) ?: return ""
        outputFormat.format(date)
    } catch (e: ParseException) {
        throw IllegalArgumentException("Not a valid datetime format")
    }
}

fun String.createImageUrl() = MovieConstant.IMAGE_BASE_URL_POSTER + this
