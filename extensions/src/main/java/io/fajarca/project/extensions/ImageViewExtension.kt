package io.fajarca.project.extensions

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import io.fajarca.project.config.constant.MovieConstant

fun ImageView.loadBackdropImage(imageUrl: String) {
    if (imageUrl.isEmpty()) return
    Glide.with(this.context)
        .load(imageUrl)
        .error(ContextCompat.getDrawable(this.context, R.drawable.ic_broken_image))
        .placeholder(ColorDrawable(Color.LTGRAY))
        .transition(DrawableTransitionOptions.withCrossFade())
        .thumbnail(MovieConstant.GLIDE_THUMBNAIL_SIZE_MULTIPLIER)
        .apply(RequestOptions.fitCenterTransform())
        .into(this)
}

fun ImageView.loadImage(imageUrl: String) {
    if (imageUrl.isEmpty()) return
    Glide.with(this.context)
        .load(imageUrl.createImageUrl())
        .error(ContextCompat.getDrawable(this.context, R.drawable.ic_broken_image))
        .placeholder(ColorDrawable(Color.LTGRAY))
        .transition(DrawableTransitionOptions.withCrossFade())
        .thumbnail(MovieConstant.GLIDE_THUMBNAIL_SIZE_MULTIPLIER)
        .apply(RequestOptions.fitCenterTransform())
        .into(this)
}

fun ImageView.loadImageRounded(imageUrl: String, roundedValue: Int = 24) {
    if (imageUrl.isEmpty()) return
    Glide.with(this.context)
        .load(imageUrl.createImageUrl())
        .error(ContextCompat.getDrawable(this.context, R.drawable.ic_broken_image))
        .placeholder(ColorDrawable(Color.LTGRAY))
        .transition(DrawableTransitionOptions.withCrossFade())
        .transform(RoundedCorners(roundedValue))
        .into(this)
}
