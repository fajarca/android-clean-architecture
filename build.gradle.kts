// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {
    repositories {
        google()
        jcenter {
            content {
                // just allow to include kotlinx projects
                // detekt needs 'kotlinx-html' for the html report
                includeGroup("org.jetbrains.kotlinx")
            }
        }
    }
    dependencies {
        classpath(BuildPlugins.androidGradlePlugin)
        classpath(BuildPlugins.kotlinGradlePlugin)
        classpath(BuildPlugins.detektPlugin)
        classpath(BuildPlugins.navigationComponentPlugin)
        classpath(BuildPlugins.googleServicesPlugin)
        classpath(BuildPlugins.appDistributionPlugin)
        classpath(BuildPlugins.hiltPlugin)
    }
}

plugins {
    id("io.gitlab.arturbosch.detekt").version("1.10.0")
    id("org.jlleitschuh.gradle.ktlint").version("9.2.1")
}

allprojects {
    repositories {
        google()
        jcenter()
    }
    apply(plugin = "org.jlleitschuh.gradle.ktlint")
}

tasks.register("clean").configure {
    delete("build")
}

detekt {
    failFast = true
    buildUponDefaultConfig = true // preconfigure defaults
    config = files("$projectDir/config/detekt/detekt.yml")
    reports {
        html.enabled = true // observe findings in your browser with structure and code snippets
        xml.enabled = true // checkstyle like format mainly for integrations like Jenkins
        txt.enabled = true // similar to the console output, contains issue signature to manually edit baseline files
    }
}
