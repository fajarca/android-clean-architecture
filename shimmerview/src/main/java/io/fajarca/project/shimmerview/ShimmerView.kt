package io.fajarca.project.shimmerview

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.LayoutRes
import com.facebook.shimmer.ShimmerFrameLayout

class ShimmerView : ShimmerFrameLayout {

    private lateinit var container: LinearLayout

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context)
    }

    private fun init(context: Context) {
        val view = View.inflate(context, R.layout.shimmer_container, this)
        container = view.findViewById(R.id.container) as LinearLayout
    }

    fun start(numberOfPlaceholderItem: Int, @LayoutRes layoutResId: Int) {
        createItem(numberOfPlaceholderItem, layoutResId)
        startShimmer()
        this.visibility = View.VISIBLE
        invalidate()
    }

    fun stop() {
        stopShimmer()
        this.visibility = View.GONE
    }

    private fun createItem(numberOfPlaceholderItem: Int, @LayoutRes layoutResId: Int) {
        repeat(numberOfPlaceholderItem) {
            val placeholder = inflate(context, layoutResId, null)
            container.addView(placeholder)
        }
        invalidate()
    }
}
