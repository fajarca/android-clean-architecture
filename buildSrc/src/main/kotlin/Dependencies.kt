const val kotlinVersion = "1.3.72"

object AppConfiguration {
    const val id = "io.fajarca.project.moviemate"
    const val versionCode = 1
    const val versionName = "1.0"
    const val minSdk = 21
    const val compileSdk = 29
    const val targetSdk = compileSdk
}

object BuildPlugins {

    object Versions {
        const val buildToolsVersion = "4.0.0"
        const val detekt = "1.1.1"
        const val navigationComponentSafeArgs = "2.2.0"
        const val googlePlayServices = "4.3.3"
        const val appDistribution = "2.0.0"
        const val hilt = "2.28-alpha"
    }

    const val androidGradlePlugin = "com.android.tools.build:gradle:${Versions.buildToolsVersion}"
    const val kotlinGradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion"
    const val detektPlugin = "io.gitlab.arturbosch.detekt:detekt-gradle-plugin:${Versions.detekt}"
    const val navigationComponentPlugin =
        "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.navigationComponentSafeArgs}"
    const val googleServicesPlugin = "com.google.gms:google-services:${Versions.googlePlayServices}"
    const val appDistributionPlugin =
        "com.google.firebase:firebase-appdistribution-gradle:${Versions.appDistribution}"
    const val hiltPlugin =
        "com.google.dagger:hilt-android-gradle-plugin:${Versions.hilt}"

    const val androidApplication = "com.android.application"
    const val kotlinAndroid = "kotlin-android"
    const val kotlinAndroidExtensions = "kotlin-android-extensions"
    const val kotlinKapt = "kotlin-kapt"
    const val androidLibrary = "com.android.library"
    const val navigationSafeArgs = "androidx.navigation.safeargs.kotlin"
    const val playServices = "com.google.gms.google-services"
    const val appDistribution = "com.google.firebase.appdistribution"
    const val detekt = "io.gitlab.arturbosch.detekt"
    const val hilt = "dagger.hilt.android.plugin"
}

object Libraries {
    private object Versions {
        const val gradle = "4.0.0"
        const val navigation = "2.3.0"
        const val appcompat = "1.2.0"
        const val material = "1.1.0"
        const val cardview = "1.0.0"
        const val recyclerview = "1.1.0"
        const val kotlin = "1.3.72"
        const val timber = "4.7.1"
        const val retrofit = "2.7.0"
        const val loggingInterceptor = "4.3.1"
        const val glide = "4.11.0"
        const val lifecycle = "2.2.0"
        const val dagger = "2.24"
        const val constraintLayout = "1.1.3"
        const val shimmer = "0.5.0"
        const val coroutineCore = "1.3.4"
        const val coroutineAndroid = "1.3.4"
        const val detekt = "1.1.1"
        const val lottie = "3.4.0"
        const val hilt = "2.28-alpha"
        const val hiltViewModel = "1.0.0-alpha01"
        const val paging = "3.0.0-alpha04"
    }


    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"
    const val constraintLayout =
        "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    const val recyclerview = "androidx.recyclerview:recyclerview:${Versions.recyclerview}"
    const val cardview = "androidx.cardview:cardview:${Versions.cardview}"
    const val appcompat = "androidx.appcompat:appcompat:${Versions.appcompat}"
    const val material = "com.google.android.material:material:${Versions.material}"
    const val navigation_fragment_ktx =
        "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
    const val navigation_ktx = "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"
    const val lifecycleExtensions = "androidx.lifecycle:lifecycle-extensions:${Versions.lifecycle}"
    const val lifecycleCompiler = "androidx.lifecycle:lifecycle-compiler:${Versions.lifecycle}"

    const val dagger = "com.google.dagger:dagger:${Versions.dagger}"
    const val daggerAndroid = "com.google.dagger:dagger-android:${Versions.dagger}"
    const val daggerAndroidSupport = "com.google.dagger:dagger-android-support:${Versions.dagger}"
    const val daggerCompiler = "com.google.dagger:dagger-compiler:${Versions.dagger}"
    const val daggerAndroidCompiler =
        "com.google.dagger:dagger-android-processor:${Versions.dagger}"

    const val hilt = "com.google.dagger:hilt-android:${Versions.hilt}"
    const val hiltCompiler = "com.google.dagger:hilt-android-compiler:${Versions.hilt}"

    const val hiltViewModel = "androidx.hilt:hilt-lifecycle-viewmodel:${Versions.hiltViewModel}"
    const val hiltViewModelCompiler = "androidx.hilt:hilt-compiler:${Versions.hiltViewModel}"

    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofitMoshiConverter = "com.squareup.retrofit2:converter-moshi:${Versions.retrofit}"
    const val loggingInterceptor =
        "com.squareup.okhttp3:logging-interceptor:${Versions.loggingInterceptor}"

    const val timber = "com.jakewharton.timber:timber:${Versions.timber}"

    const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    const val glideCompiler = "com.github.bumptech.glide:compiler:${Versions.glide}"

    const val shimmer = "com.facebook.shimmer:shimmer:${Versions.shimmer}"
    const val coroutine = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutineCore}"
    const val coroutineAndroid =
        "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutineAndroid}"
    const val detektFormatting = "io.gitlab.arturbosch.detekt:detekt-formatting:${Versions.detekt}"
    const val lottie = "com.airbnb.android:lottie:${Versions.lottie}"
    const val paging = "androidx.paging:paging-runtime:${Versions.paging}"
}

object FirebaseLibraries {
    private object Versions {
        const val analytics = "17.5.0"
    }

    const val firebaseAnalytics = "com.google.firebase:firebase-analytics:${Versions.analytics}"
}

object TestLibraries {
    private object Versions {
        const val junit4 = "4.12"
        const val androidJunit = "1.1.1"
        const val coroutine = "1.3.2"
        const val mockito = "2.28.2"
        const val espresso = "3.2.0"
        const val fragment = "1.2.5"
        const val navigation = "2.3.0"
        const val activityTestRule = "1.2.0"
        const val mockWebServer = "4.3.1"
        const val hiltAndroid = "2.28-alpha"
    }

    const val coroutine = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutine}"
    const val mockito = "org.mockito:mockito-core:${Versions.mockito}"
    const val junit = "junit:junit:${Versions.junit4}"
    const val androidJunit = "androidx.test.ext:junit:${Versions.androidJunit}"
    const val fragment = "androidx.fragment:fragment-testing:${Versions.fragment}"
    const val navigation = "androidx.navigation:navigation-testing:${Versions.navigation}"
    const val espresso = "androidx.test.espresso:espresso-core:${Versions.espresso}"
    const val activityTestRule = "androidx.test:rules:${Versions.activityTestRule}"
    const val mockWebServer = "com.squareup.okhttp3:mockwebserver:${Versions.mockWebServer}"
    const val idlingResource =
        "androidx.test.espresso:espresso-idling-resource:${Versions.espresso}"
    const val hiltAndroid = "com.google.dagger:hilt-android-testing:${Versions.hiltAndroid}"
    const val hiltAndroidCompiler = "com.google.dagger:hilt-android-compiler:${Versions.hiltAndroid}"
}

object Modules {
    const val stateView = ":stateview"
    const val shimmerview = ":shimmerview"
    const val extensions = ":extensions"
    const val config = ":config"
    const val resource = ":resource"
}