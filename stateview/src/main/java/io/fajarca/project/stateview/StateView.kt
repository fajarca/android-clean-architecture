package io.fajarca.project.stateview

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.lottie.LottieAnimationView

class StateView : ConstraintLayout {

    private lateinit var tvStatus: TextView
    private lateinit var lottie: LottieAnimationView
    private lateinit var btnRetry: Button

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init()
    }

    private fun init() {
        val view = View.inflate(context, R.layout.layout_state_view, this)
        lottie = view.findViewById(R.id.lottie)
        tvStatus = view.findViewById(R.id.tvStatus)
        btnRetry = view.findViewById(R.id.btnRetry)
        invalidate()
    }

    fun showEmptyDataNotice(onEmptyDataMessage: String = "No data found") {
        visible()
        lottie.setAnimation("lottie_search_empty.json")
        lottie.playAnimation()
        tvStatus.text = onEmptyDataMessage
        hideRetryButton()
    }

    fun showErrorNotice(
        errorMessage: String = "We're sorry. Something is not working. Please try again later",
        retryAction: () -> Unit = {}
    ) {
        visible()
        lottie.setAnimation("lottie_error.json")
        lottie.playAnimation()
        tvStatus.text = errorMessage
        showRetryButton { retryAction() }
    }

    fun showNoInternetConnectionNotice(
        errorMessage: String = "No internet connection. Please check your network connection and try again",
        retryAction: () -> Unit = {}
    ) {
        visible()
        lottie.setAnimation("lottie_no_internet_connection.json")
        lottie.playAnimation()
        tvStatus.text = errorMessage
        showRetryButton { retryAction() }
    }

    private fun visible() {
        this.visibility = View.VISIBLE
    }

    fun dismiss() {
        this.visibility = View.GONE
    }

    private fun showRetryButton(retryAction: () -> Unit = {}) {
        btnRetry.visibility = View.VISIBLE
        btnRetry.setOnClickListener { retryAction() }
    }

    private fun hideRetryButton() {
        btnRetry.visibility = View.GONE
    }
}
