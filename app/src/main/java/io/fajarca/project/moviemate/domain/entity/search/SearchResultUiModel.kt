package io.fajarca.project.moviemate.domain.entity.search

import io.fajarca.project.moviemate.abstraction.BaseItemModel
import io.fajarca.project.moviemate.presentation.factory.ItemTypeFactory

data class SearchResultUiModel(
    val adult: Boolean,
    val id: Int,
    val mediaType: MediaType,
    val name: String,
    val imageUrl: String,
    val voteAverage: Float,
    val releaseDate: String
) : BaseItemModel() {
    override fun type(itemTypeFactory: ItemTypeFactory): Int {
        return itemTypeFactory.type(this)
    }
}
