package io.fajarca.project.moviemate.data.source

import com.squareup.moshi.Moshi
import io.fajarca.project.moviemate.data.vo.HttpResult
import io.fajarca.project.moviemate.data.vo.Result
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import retrofit2.HttpException

open class RemoteDataSource {

    companion object {
        private const val SERVER_ERROR_BEGIN_CODE = 500
        private const val SERVER_ERROR_END_CODE = 599
        private const val CLIENT_ERROR_BEGIN_CODE = 400
        private const val CLIENT_ERROR_END_CODE = 451
    }

    open suspend fun <T> safeApiCall(
        dispatcher: CoroutineDispatcher,
        apiCall: suspend () -> T
    ): Result<T> {
        return withContext(dispatcher) {
            try {
                Result.Success(apiCall.invoke())
            } catch (exception: HttpException) {
                val result = when (exception.code()) {
                    in CLIENT_ERROR_BEGIN_CODE..CLIENT_ERROR_END_CODE -> parseHttpError(
                        exception
                    )
                    in SERVER_ERROR_BEGIN_CODE..SERVER_ERROR_END_CODE -> error(
                        HttpResult.SERVER_ERROR,
                        exception.code(),
                        "Server error"
                    )
                    else -> error(
                        HttpResult.NOT_DEFINED,
                        exception.code(),
                        "Undefined error"
                    )
                }
                result
            } catch (exception: UnknownHostException) {
                error(
                    HttpResult.NO_CONNECTION,
                    null,
                    "No internet connection"
                )
            } catch (exception: SocketTimeoutException) {
                error(HttpResult.TIMEOUT, null, "Slow connection")
            } catch (exception: IOException) {
                error(HttpResult.BAD_RESPONSE, null, exception.message)
            }
        }
    }

    private fun error(cause: HttpResult, code: Int?, errorMessage: String?): Result.Error {
        return Result.Error(cause, code, errorMessage)
    }

    private fun parseHttpError(throwable: HttpException): Result<Nothing> {
        return try {
            val errorBody = throwable.response()?.errorBody()?.string() ?: "Unknown HTTP error body"
            val moshi = Moshi.Builder().build()
            val adapter = moshi.adapter(Object::class.java)
            val errorMessage = adapter.fromJson(errorBody)
            error(HttpResult.CLIENT_ERROR, throwable.code(), errorMessage.toString())
        } catch (exception: IOException) {
            error(HttpResult.CLIENT_ERROR, throwable.code(), exception.localizedMessage)
        }
    }
}
