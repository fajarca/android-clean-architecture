package io.fajarca.project.moviemate.presentation.screen.moviedetail.viewholder

import io.fajarca.project.moviemate.R
import io.fajarca.project.moviemate.abstraction.BaseViewHolder
import io.fajarca.project.moviemate.databinding.ItemGenreBinding
import io.fajarca.project.moviemate.domain.entity.ItemClickListener
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieDetail

class GenreViewHolder(private val binding: ItemGenreBinding) : BaseViewHolder<MovieDetail.Genre>(binding) {

    companion object {
        const val LAYOUT = R.layout.item_genre
    }

    override fun bind(item: MovieDetail.Genre, clickListener: ItemClickListener) {
        binding.name = item.name
        binding.executePendingBindings()
    }
}
