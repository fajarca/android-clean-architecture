package io.fajarca.project.moviemate.presentation.screen.movies

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.fajarca.project.moviemate.R
import io.fajarca.project.moviemate.abstraction.BaseViewHolder
import io.fajarca.project.moviemate.databinding.ItemMovieBinding
import io.fajarca.project.moviemate.databinding.ItemMoviesBinding
import io.fajarca.project.moviemate.domain.entity.ItemClickListener
import io.fajarca.project.moviemate.domain.entity.movielist.Movie
import io.fajarca.project.moviemate.domain.entity.movielist.MovieSection

class MovieViewHolder(private val binding: ItemMoviesBinding) :
    BaseViewHolder<MovieSection>(binding) {

    private lateinit var adapter: MovieAdapter

    companion object {
        const val LAYOUT = R.layout.item_movies
    }

    override fun bind(item: MovieSection, clickListener: ItemClickListener) {
        binding.tvHeader.text = item.header
        adapter = MovieAdapter(item, clickListener)
        binding.recyclerViewMovie.layoutManager =
            LinearLayoutManager(itemView.context, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerViewMovie.adapter = adapter
        binding.recyclerViewMovie.setHasFixedSize(true)
        binding.recyclerViewMovie.isNestedScrollingEnabled = false
    }

    class MovieAdapter(
        private val items: MovieSection,
        private val clickListener: ItemClickListener
    ) : RecyclerView.Adapter<MovieAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder.create(parent)
        }

        override fun getItemCount(): Int = items.movies.size

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bind(items.movies[position], clickListener)
        }

        class ViewHolder(private val binding: ItemMovieBinding) :
            RecyclerView.ViewHolder(binding.root) {

            fun bind(movie: Movie, clickListener: ItemClickListener) {
                binding.movie = movie
                binding.root.setOnClickListener { clickListener.onClick(movie) }
            }

            companion object {
                fun create(viewGroup: ViewGroup): ViewHolder {
                    val layoutInflater = LayoutInflater.from(viewGroup.context)
                    val binding = ItemMovieBinding.inflate(layoutInflater, viewGroup, false)
                    return ViewHolder(binding)
                }
            }
        }
    }
}
