package io.fajarca.project.moviemate.data.source

import io.fajarca.project.moviemate.data.response.search.MultiSearchDto
import io.fajarca.project.moviemate.data.service.SearchService
import io.fajarca.project.moviemate.data.vo.Result
import javax.inject.Inject
import kotlinx.coroutines.CoroutineDispatcher

class SearchRemoteDataSource @Inject constructor(private val searchService: SearchService) :
    RemoteDataSource() {
    suspend fun multiSearch(
        dispatcher: CoroutineDispatcher,
        query: String,
        page: Int,
        includeAdult: Boolean
    ): Result<MultiSearchDto> {
        return safeApiCall(dispatcher) { searchService.multiSearch(query, page, includeAdult) }
    }
}