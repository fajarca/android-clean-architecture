package io.fajarca.project.moviemate.data.source

import io.fajarca.project.moviemate.data.response.movies.CastDto
import io.fajarca.project.moviemate.data.response.movies.MovieDetailDto
import io.fajarca.project.moviemate.data.response.movies.MovieImagesDto
import io.fajarca.project.moviemate.data.response.movies.MovieVideosDto
import io.fajarca.project.moviemate.data.response.movies.NowPlayingMovieDto
import io.fajarca.project.moviemate.data.response.movies.PopularMovieDto
import io.fajarca.project.moviemate.data.response.movies.SimilarMoviesDto
import io.fajarca.project.moviemate.data.response.movies.TopRatedMovieDto
import io.fajarca.project.moviemate.data.service.MovieService
import io.fajarca.project.moviemate.data.vo.Result
import javax.inject.Inject
import kotlinx.coroutines.CoroutineDispatcher

class MovieRemoteDataSource @Inject constructor(private val movieService: MovieService) : RemoteDataSource() {

    suspend fun getNowPlayingMovies(dispatcher: CoroutineDispatcher): Result<NowPlayingMovieDto> {
        return safeApiCall(dispatcher) { movieService.nowPlaying() }
    }
    suspend fun getMovieDetail(dispatcher: CoroutineDispatcher, movieId: Int): Result<MovieDetailDto> {
        return safeApiCall(dispatcher) { movieService.detail(movieId) }
    }

    suspend fun getPopularMovies(dispatcher: CoroutineDispatcher): Result<PopularMovieDto> {
        return safeApiCall(dispatcher) { movieService.popular() }
    }

    suspend fun getTopRatedMovies(dispatcher: CoroutineDispatcher): Result<TopRatedMovieDto> {
        return safeApiCall(dispatcher) { movieService.topRated() }
    }
    suspend fun getMovieCasts(dispatcher: CoroutineDispatcher, movieId: Int): Result<CastDto> {
        return safeApiCall(dispatcher) { movieService.credits(movieId) }
    }
    suspend fun getMovieImages(dispatcher: CoroutineDispatcher, movieId: Int): Result<MovieImagesDto> {
        return safeApiCall(dispatcher) { movieService.images(movieId) }
    }
    suspend fun getSimilarMovies(dispatcher: CoroutineDispatcher, movieId: Int): Result<SimilarMoviesDto> {
        return safeApiCall(dispatcher) { movieService.similar(movieId) }
    }
    suspend fun getMovieVideos(dispatcher: CoroutineDispatcher, movieId: Int): Result<MovieVideosDto> {
        return safeApiCall(dispatcher) { movieService.videos(movieId) }
    }
}
