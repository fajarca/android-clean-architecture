package io.fajarca.project.moviemate.presentation.screen.search

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.core.view.OnApplyWindowInsetsListener
import androidx.core.view.ViewCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import io.fajarca.project.extensions.setMarginTop
import io.fajarca.project.moviemate.R
import io.fajarca.project.moviemate.abstraction.BaseFragment
import io.fajarca.project.moviemate.databinding.FragmentSearchBinding
import io.fajarca.project.moviemate.domain.entity.search.SearchResultUiModel
import io.fajarca.project.moviemate.presentation.MainActivity
import io.fajarca.project.moviemate.presentation.utils.DebouncingTextListener
import io.fajarca.project.moviemate.presentation.utils.RecyclerviewDividerItemDecoration
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SearchFragment : BaseFragment<FragmentSearchBinding>(),
    SearchAdapter.SearchResultClickListener {

    override fun getLayoutResourceId() = R.layout.fragment_search
    private val viewModel: SearchViewModel by viewModels()

    private val adapter by lazy { SearchAdapter(this) }
    private var searchQuery = ""
    private lateinit var searchView: SearchView

    companion object {
        private const val SEARCH_VIEW_DEBOUNCE_INTERVAL_IN_MILLIS = 700L
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
        initRecyclerView()
        applyTopInset()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_search, menu)
        val searchMenuItem = menu.findItem(R.id.action_search)
        searchMenuItem.expandActionView()
        val searchManager =
            requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView = searchMenuItem.actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName))
        searchView.queryHint = "Search movie, series, cast..."
        if (searchQuery.isNotEmpty()) searchView.setQuery(searchQuery, false)
        searchView.setIconifiedByDefault(false)
        searchView.isIconified = false
        searchView.maxWidth = Int.MAX_VALUE
        searchView.setOnQueryTextListener(
            DebouncingTextListener(
                SEARCH_VIEW_DEBOUNCE_INTERVAL_IN_MILLIS,
                lifecycleScope,
                onTextChanged = { query ->
                    startMultiSearch(query)
                    searchQuery = query
                },
                onQuerySubmitted = { query ->
                    searchView.clearFocus()
                    searchQuery = query
                }
            )
        )
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun applyTopInset() {
        val listener = OnApplyWindowInsetsListener { _, insets ->
            binding.appBarLayout.setMarginTop(insets?.systemWindowInsetTop ?: 0)
            insets?.consumeSystemWindowInsets()
        }

        ViewCompat.setOnApplyWindowInsetsListener(binding.root, listener)
    }

    private fun setupToolbar() {
        (requireActivity() as MainActivity).setSupportActionBar(binding.toolbar)
        (requireActivity() as MainActivity).supportActionBar?.setDisplayShowTitleEnabled(false)
        (requireActivity() as MainActivity).supportActionBar?.title = null
    }

    private fun startMultiSearch(searchQuery: String) {
        lifecycleScope.launch {
            viewModel.search(searchQuery, true).collectLatest {
                adapter.submitData(it)
            }
        }
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(requireActivity())
        binding.recyclerView.layoutManager = layoutManager
        binding.recyclerView.addItemDecoration(RecyclerviewDividerItemDecoration(requireActivity()))
        binding.recyclerView.adapter =
            adapter.withLoadStateFooter(footer = SearchLoadStateAdapter())
    }

    override fun onSearchResultPressed(searchResult: SearchResultUiModel) {
        findNavController().navigate(
            SearchFragmentDirections.actionSearchFragmentToFragmentMovieDetail(
                searchResult.id
            )
        )
        searchView.setQuery(searchQuery, false)
    }
}
