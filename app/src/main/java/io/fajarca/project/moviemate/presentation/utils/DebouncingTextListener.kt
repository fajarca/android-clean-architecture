package io.fajarca.project.moviemate.presentation.utils

import androidx.appcompat.widget.SearchView
import androidx.lifecycle.LifecycleCoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class DebouncingTextListener(
    private val debouncePeriodMillis: Long = 300L,
    private val lifecycleCoroutineScope: LifecycleCoroutineScope,
    private val onTextChanged: (String) -> Unit,
    private val onQuerySubmitted: (String) -> Unit
) : SearchView.OnQueryTextListener {

    private var searchJob: Job? = null

    override fun onQueryTextSubmit(query: String?): Boolean {
        searchJob = lifecycleCoroutineScope.launch {
            delay(debouncePeriodMillis)
            val textQuery = query.toString().trim()
            onTextChanged(textQuery)
        }
        onQuerySubmitted(query.toString().trim())
        searchJob?.cancel()
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        searchJob?.cancel()
        searchJob = lifecycleCoroutineScope.launch {
            delay(debouncePeriodMillis)
            val query = newText.toString().trim()
            onTextChanged(query)
        }
        return true
    }
}
