package io.fajarca.project.moviemate.domain.entity.moviedetail

data class MovieDetailData(
    val detail: MovieDetailUiModel,
    val casts: List<Cast>,
    val images: List<MovieImage>,
    val similar: List<SimilarMovie>,
    val videos: List<MovieVideo>
)
