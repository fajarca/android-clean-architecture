package io.fajarca.project.moviemate.presentation.screen.moviedetail.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.fajarca.project.moviemate.R
import io.fajarca.project.moviemate.abstraction.BaseViewHolder
import io.fajarca.project.moviemate.databinding.ItemSimilarBinding
import io.fajarca.project.moviemate.databinding.ItemSimilarContainerBinding
import io.fajarca.project.moviemate.domain.entity.ItemClickListener
import io.fajarca.project.moviemate.domain.entity.moviedetail.SimilarMovie
import io.fajarca.project.moviemate.domain.entity.moviedetail.SimilarMovieSection

class SimilarViewHolder(private val binding: ItemSimilarContainerBinding) :
    BaseViewHolder<SimilarMovieSection>(binding) {

    private lateinit var adapter: SimilarMovieAdapter

    companion object {
        const val LAYOUT = R.layout.item_similar
    }

    override fun bind(item: SimilarMovieSection, clickListener: ItemClickListener) {
        binding.tvHeader.text = item.header
        adapter = SimilarMovieAdapter(item.similars, clickListener)
        binding.recyclerView.layoutManager =
            LinearLayoutManager(itemView.context, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerView.adapter = adapter
        binding.recyclerView.setHasFixedSize(true)
        binding.recyclerView.isNestedScrollingEnabled = false
        binding.executePendingBindings()
    }

    class SimilarMovieAdapter(
        private val items: List<SimilarMovie>,
        private val clickListener: ItemClickListener
    ) : RecyclerView.Adapter<SimilarMovieAdapter.SimilarMovieViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimilarMovieViewHolder {
            return SimilarMovieViewHolder.create(parent)
        }

        override fun getItemCount(): Int = items.size

        override fun onBindViewHolder(holder: SimilarMovieViewHolder, position: Int) {
            holder.bind(items[position], clickListener)
        }

        class SimilarMovieViewHolder(private val binding: ItemSimilarBinding) :
            RecyclerView.ViewHolder(binding.root) {

            fun bind(similarMovie: SimilarMovie, clickListener: ItemClickListener) {
                binding.similar = similarMovie
                binding.root.setOnClickListener { clickListener.onClick(similarMovie) }
                binding.executePendingBindings()
            }

            companion object {
                fun create(viewGroup: ViewGroup): SimilarMovieViewHolder {
                    val layoutInflater = LayoutInflater.from(viewGroup.context)
                    val binding = ItemSimilarBinding.inflate(layoutInflater, viewGroup, false)
                    return SimilarMovieViewHolder(binding)
                }
            }
        }
    }
}
