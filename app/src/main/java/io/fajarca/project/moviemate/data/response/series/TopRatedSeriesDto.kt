package io.fajarca.project.moviemate.data.response.series

import com.squareup.moshi.Json

data class TopRatedSeriesDto(
    @field:Json(name = "results")
    val results: List<Result?> = emptyList()
) {
    data class Result(
        @field:Json(name = "backdrop_path")
        val backdropPath: String? = null,
        @field:Json(name = "first_air_date")
        val firstAirDate: String? = null,
        @field:Json(name = "id")
        val id: Int? = null,
        @field:Json(name = "name")
        val name: String? = null,
        @field:Json(name = "original_language")
        val originalLanguage: String? = null,
        @field:Json(name = "original_name")
        val originalName: String? = null,
        @field:Json(name = "overview")
        val overview: String? = null,
        @field:Json(name = "popularity")
        val popularity: Float? = null,
        @field:Json(name = "poster_path")
        val posterPath: String? = null,
        @field:Json(name = "vote_average")
        val voteAverage: Float? = null,
        @field:Json(name = "vote_count")
        val voteCount: Int? = null
    )
}
