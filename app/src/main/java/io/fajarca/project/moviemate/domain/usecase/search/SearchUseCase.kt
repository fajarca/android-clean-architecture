package io.fajarca.project.moviemate.domain.usecase.search

import androidx.paging.PagingData
import androidx.paging.map
import io.fajarca.project.moviemate.domain.entity.search.MediaType
import io.fajarca.project.moviemate.domain.entity.search.SearchResult
import io.fajarca.project.moviemate.domain.entity.search.SearchResultUiModel
import io.fajarca.project.moviemate.domain.repository.SearchRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class SearchUseCase @Inject constructor(private val repository: SearchRepository) {

    operator fun invoke(query: String, includeAdult: Boolean): Flow<PagingData<SearchResultUiModel>> {
        val apiResult = repository.multiSearch(query, includeAdult)
            .map { pagingData -> pagingData.map { handleData(it) } }
        return apiResult
    }


    private fun handleData(searchResult: SearchResult): SearchResultUiModel {
        return when (searchResult.mediaType) {
            "person" -> createUiModel(searchResult)
            "movie" -> createUiModel(searchResult)
            "tv" -> createUiModel(searchResult)
            else -> throw IllegalArgumentException("Unknown media type")
        }
    }

    private fun createUiModel(searchResult: SearchResult): SearchResultUiModel {
        return when (searchResult.mediaType) {
            "person" -> SearchResultUiModel(
                searchResult.adult,
                searchResult.id,
                MediaType.PERSON,
                searchResult.name,
                searchResult.profilePath,
                searchResult.voteAverage,
                searchResult.releaseDate
            )
            "movie" -> SearchResultUiModel(
                searchResult.adult,
                searchResult.id,
                MediaType.MOVIE,
                searchResult.title,
                searchResult.posterPath,
                searchResult.voteAverage,
                searchResult.releaseDate
            )
            "tv" -> SearchResultUiModel(
                searchResult.adult,
                searchResult.id,
                MediaType.SERIES,
                searchResult.name,
                searchResult.posterPath,
                searchResult.voteAverage,
                searchResult.releaseDate
            )
            else -> SearchResultUiModel(
                searchResult.adult,
                searchResult.id,
                MediaType.MOVIE,
                searchResult.title,
                searchResult.posterPath,
                searchResult.voteAverage,
                searchResult.releaseDate
            )
        }
    }
}
