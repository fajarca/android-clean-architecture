package io.fajarca.project.moviemate.presentation.screen.movies

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import io.fajarca.project.extensions.gone
import io.fajarca.project.extensions.loadBackdropImage
import io.fajarca.project.extensions.visible
import io.fajarca.project.moviemate.R
import io.fajarca.project.moviemate.abstraction.BaseFragment
import io.fajarca.project.moviemate.abstraction.BaseItemModel
import io.fajarca.project.moviemate.abstraction.BaseRecyclerViewAdapter
import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.databinding.FragmentMoviesBinding
import io.fajarca.project.moviemate.domain.entity.ItemClickListener
import io.fajarca.project.moviemate.domain.entity.movielist.Movie
import io.fajarca.project.moviemate.helper.IdlingResourceWrapper
import io.fajarca.project.moviemate.presentation.factory.ItemTypeFactoryImpl
import io.fajarca.project.moviemate.presentation.screen.series.TvSeriesViewModel

@AndroidEntryPoint
class MoviesFragment : BaseFragment<FragmentMoviesBinding>(),
    ItemClickListener {

    override fun getLayoutResourceId() = R.layout.fragment_movies
    private val viewModel: MoviesViewModel by viewModels()

    private val adapter by lazy {
        BaseRecyclerViewAdapter(
            ItemTypeFactoryImpl(),
            arrayListOf(),
            this
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        observeMoviesResult()
        observeBanner()
        viewModel.getMovies()
    }

    private fun observeMoviesResult() {
        viewModel.movies.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Result.Loading -> {
                    IdlingResourceWrapper.increment()
                    binding.recyclerView.gone()
                    binding.shimmerView.start(1, R.layout.placeholder_movie_fragment)
                }
                is Result.Success -> {
                    IdlingResourceWrapper.decrement()
                    binding.recyclerView.visible()
                    adapter.refreshItems(it.data)
                    binding.shimmerView.stop()
                }
            }
        })
    }

    private fun observeBanner() {
        viewModel.banner.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Result.Success -> {
                    IdlingResourceWrapper.decrement()
                    binding.ivMovie.loadBackdropImage(it.data.imageUrl)
                    binding.tvTitle.text = it.data.title
                }
                is Result.Empty -> {
                    IdlingResourceWrapper.decrement()
                }
            }
        })
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(requireActivity())
        binding.recyclerView.layoutManager = layoutManager
        binding.recyclerView.adapter = adapter
    }

    override fun onClick(item: BaseItemModel) {
        val movie = item as Movie
        findNavController().navigate(
            MoviesFragmentDirections.actionFragmentMoviesToFragmentMovieDetail(
                movie.id
            )
        )
    }
}
