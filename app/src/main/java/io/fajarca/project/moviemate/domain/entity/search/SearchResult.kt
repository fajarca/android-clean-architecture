package io.fajarca.project.moviemate.domain.entity.search

data class SearchResult(
    val adult: Boolean,
    val firstAirDate: String,
    val id: Int,
    val mediaType: String,
    val name: String,
    val title: String,
    val posterPath: String,
    val releaseDate: String,
    val voteAverage: Float,
    val profilePath: String
)
