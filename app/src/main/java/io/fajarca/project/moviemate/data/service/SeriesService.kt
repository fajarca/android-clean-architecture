package io.fajarca.project.moviemate.data.service

import io.fajarca.project.moviemate.data.response.series.OnTheAirDto
import io.fajarca.project.moviemate.data.response.series.PopularSeriesDto
import io.fajarca.project.moviemate.data.response.series.TopRatedSeriesDto
import retrofit2.http.GET

interface SeriesService {

    @GET("tv/popular")
    suspend fun popular(): PopularSeriesDto

    @GET("tv/top_rated")
    suspend fun topRated(): TopRatedSeriesDto

    @GET("tv/on_the_air")
    suspend fun onTheAir(): OnTheAirDto
}
