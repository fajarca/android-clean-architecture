package io.fajarca.project.moviemate.data.mapper.movie

import io.fajarca.project.moviemate.abstraction.Mapper
import io.fajarca.project.moviemate.data.response.movies.CastDto
import io.fajarca.project.moviemate.domain.entity.moviedetail.Cast
import javax.inject.Inject

class MovieCastMapper @Inject constructor() : Mapper<CastDto, List<Cast>>() {

    override fun map(input: CastDto): List<Cast> {
        return input.casts.map {
            Cast(
                it?.castId ?: 0,
                it?.character ?: "",
                it?.name ?: "",
                it?.profilePath ?: "",
                it?.order ?: 0
            )
        }
    }
}
