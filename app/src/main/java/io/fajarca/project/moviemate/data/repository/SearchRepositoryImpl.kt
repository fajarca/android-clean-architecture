package io.fajarca.project.moviemate.data.repository

import androidx.paging.*
import io.fajarca.project.moviemate.data.dispatcher.CoroutineDispatcherProvider
import io.fajarca.project.moviemate.data.mapper.search.MultiSearchMapper
import io.fajarca.project.moviemate.data.source.PagingSearchRemoteDataSource
import io.fajarca.project.moviemate.data.source.SearchRemoteDataSource
import io.fajarca.project.moviemate.domain.entity.search.SearchResult
import io.fajarca.project.moviemate.domain.repository.SearchRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class SearchRepositoryImpl @Inject constructor(
    private val dispatcherProvider: CoroutineDispatcherProvider,
    private val multiSearchMapper: MultiSearchMapper,
    private val remoteDataSource: SearchRemoteDataSource
) : SearchRepository {

    override fun multiSearch(
        query: String,
        includeAdult: Boolean
    ): Flow<PagingData<SearchResult>> {
        val pagingConfig = PagingConfig(20)
        val dataSource = {
            PagingSearchRemoteDataSource(
                query,
                includeAdult,
                dispatcherProvider,
                remoteDataSource
            )
        }
        return Pager(pagingConfig, null, null, dataSource)
            .flow
            .map { pagingData ->
                pagingData.map { searchResult -> multiSearchMapper.map(searchResult) }
            }
    }
}
