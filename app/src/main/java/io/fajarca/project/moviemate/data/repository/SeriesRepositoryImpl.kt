package io.fajarca.project.moviemate.data.repository

import io.fajarca.project.moviemate.data.dispatcher.CoroutineDispatcherProvider
import io.fajarca.project.moviemate.data.mapper.series.OnTheAirSeriesMapper
import io.fajarca.project.moviemate.data.mapper.series.PopularSeriesMapper
import io.fajarca.project.moviemate.data.mapper.series.TopRatedSeriesMapper
import io.fajarca.project.moviemate.data.source.SeriesRemoteDataSource
import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.domain.entity.serieslist.Series
import io.fajarca.project.moviemate.domain.repository.SeriesRepository
import javax.inject.Inject

class SeriesRepositoryImpl @Inject constructor(
    private val remoteDataSource: SeriesRemoteDataSource,
    private val dispatcher: CoroutineDispatcherProvider,
    private val popularSeriesMapper: PopularSeriesMapper,
    private val topRatedSeriesMapper: TopRatedSeriesMapper,
    private val onTheAirSeriesMapper: OnTheAirSeriesMapper
) : SeriesRepository {

    override suspend fun getPopularSeries(): Result<List<Series>> {
        val apiResult = remoteDataSource.getPopularSeries(dispatcher.io)
        return when (apiResult) {
            is Result.Success -> Result.Success(popularSeriesMapper.map(apiResult.data))
            is Result.Error -> Result.Error(apiResult.cause, apiResult.code, apiResult.errorMessage)
            else -> Result.Error()
        }
    }

    override suspend fun getTopRatedSeries(): Result<List<Series>> {
        val apiResult = remoteDataSource.getTopRatedSeries(dispatcher.io)
        return when (apiResult) {
            is Result.Success -> Result.Success(topRatedSeriesMapper.map(apiResult.data))
            is Result.Error -> Result.Error(apiResult.cause, apiResult.code, apiResult.errorMessage)
            else -> Result.Error()
        }
    }

    override suspend fun getOnTheAirSeries(): Result<List<Series>> {
        val apiResult = remoteDataSource.onTheAirSeries(dispatcher.io)
        return when (apiResult) {
            is Result.Success -> Result.Success(onTheAirSeriesMapper.map(apiResult.data))
            is Result.Error -> Result.Error(apiResult.cause, apiResult.code, apiResult.errorMessage)
            else -> Result.Error()
        }
    }
}
