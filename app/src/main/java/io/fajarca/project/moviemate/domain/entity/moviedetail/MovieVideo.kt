package io.fajarca.project.moviemate.domain.entity.moviedetail

import io.fajarca.project.moviemate.abstraction.BaseItemModel
import io.fajarca.project.moviemate.presentation.factory.ItemTypeFactory

data class MovieVideo(
    val id: String,
    val key: String,
    val name: String,
    val site: String,
    val size: Int,
    val type: String
) : BaseItemModel() {
    override fun type(itemTypeFactory: ItemTypeFactory): Int {
        return itemTypeFactory.type(this)
    }
}
