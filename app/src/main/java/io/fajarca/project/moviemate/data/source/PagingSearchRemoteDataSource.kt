package io.fajarca.project.moviemate.data.source

import androidx.paging.PagingSource
import io.fajarca.project.moviemate.data.dispatcher.CoroutineDispatcherProvider
import io.fajarca.project.moviemate.data.response.search.MultiSearchDto
import io.fajarca.project.moviemate.data.vo.Result

class PagingSearchRemoteDataSource(
    private val query: String,
    private val includeAdult: Boolean,
    private val dispatcherProvider: CoroutineDispatcherProvider,
    private val remoteDataSource: SearchRemoteDataSource
) : PagingSource<Int, MultiSearchDto.Result>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, MultiSearchDto.Result> {
        return try {
            val currentKey = params.key ?: 1
            val previousKey = if (currentKey == 1) null else currentKey - 1
            val nextKey = currentKey + 1

            val apiResult = remoteDataSource.multiSearch(dispatcherProvider.io, query, currentKey, includeAdult)
            return when (apiResult) {
                is Result.Success -> {
                    LoadResult.Page(
                        data = apiResult.data.results,
                        prevKey = previousKey,
                        nextKey = nextKey
                    )
                }
                is Result.Error -> LoadResult.Error(Exception())
                else -> LoadResult.Error(Exception())
            }

        } catch (exception: Exception) {
            LoadResult.Error(exception)
        }
    }

}