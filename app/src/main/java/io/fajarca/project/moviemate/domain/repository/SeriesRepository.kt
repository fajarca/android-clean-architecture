package io.fajarca.project.moviemate.domain.repository

import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.domain.entity.serieslist.Series

interface SeriesRepository {
    suspend fun getPopularSeries(): Result<List<Series>>
    suspend fun getTopRatedSeries(): Result<List<Series>>
    suspend fun getOnTheAirSeries(): Result<List<Series>>
}
