package io.fajarca.project.moviemate.presentation.screen.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import io.fajarca.project.extensions.loadImage
import io.fajarca.project.moviemate.databinding.ItemSearchResultBinding
import io.fajarca.project.moviemate.domain.entity.search.MediaType
import io.fajarca.project.moviemate.domain.entity.search.SearchResultUiModel

class SearchAdapter(private val listener: SearchResultClickListener) : PagingDataAdapter<SearchResultUiModel, SearchAdapter.ViewHolder>(
        diffCallback
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position) ?: return, listener)
    }

    class ViewHolder(private val binding: ItemSearchResultBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(searchResult: SearchResultUiModel, listener: SearchResultClickListener) {
            when (searchResult.mediaType) {
                MediaType.MOVIE -> {
                    binding.tvMediaType.text = "Movie"
                    binding.tvTitle.text = searchResult.name
                }
                MediaType.SERIES -> {
                    binding.tvTitle.text = searchResult.name
                    binding.tvMediaType.text = "Series"
                }
                else -> {
                    binding.tvMediaType.text = "Person"
                    binding.tvTitle.text = searchResult.name
                }
            }
            binding.tvRating.text = searchResult.voteAverage.toString()
            binding.ivPoster.loadImage(searchResult.imageUrl)
            binding.executePendingBindings()
            binding.root.setOnClickListener { listener.onSearchResultPressed(searchResult) }
            binding.executePendingBindings()
        }

        companion object {
            fun create(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemSearchResultBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

    interface SearchResultClickListener {
        fun onSearchResultPressed(searchResult: SearchResultUiModel)
    }

    companion object {
        val diffCallback = object : DiffUtil.ItemCallback<SearchResultUiModel>() {
            override fun areItemsTheSame(
                oldItem: SearchResultUiModel,
                newItem: SearchResultUiModel
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: SearchResultUiModel,
                newItem: SearchResultUiModel
            ): Boolean {
                return oldItem == newItem
            }

        }
    }
}
