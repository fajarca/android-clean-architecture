package io.fajarca.project.moviemate.data.mapper.movie

import io.fajarca.project.moviemate.abstraction.Mapper
import io.fajarca.project.moviemate.data.response.movies.MovieImagesDto
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieImage
import javax.inject.Inject

class MovieImageMapper @Inject constructor() : Mapper<MovieImagesDto, List<MovieImage>>() {

    override fun map(input: MovieImagesDto): List<MovieImage> {
        return input.posters.map {
            MovieImage(it?.aspectRatio ?: 0f, it?.filePath ?: "", it?.height ?: 0, it?.width ?: 0)
        }
    }
}
