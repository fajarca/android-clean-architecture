package io.fajarca.project.moviemate.domain.usecase.tvserieslist

import io.fajarca.project.moviemate.abstraction.UseCase
import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.domain.entity.serieslist.Series
import io.fajarca.project.moviemate.domain.repository.SeriesRepository
import javax.inject.Inject

class GetTopRatedSeriesUseCase @Inject constructor(private val repository: SeriesRepository) :
    UseCase<List<Series>, UseCase.None>() {

    override suspend fun invoke(params: None): List<Series> {
        val result = repository.getTopRatedSeries()
        return when (result) {
            is Result.Success -> result.data
            else -> emptyList()
        }
    }
}
