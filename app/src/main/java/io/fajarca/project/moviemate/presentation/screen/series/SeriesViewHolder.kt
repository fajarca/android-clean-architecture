package io.fajarca.project.moviemate.presentation.screen.series

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.fajarca.project.moviemate.R
import io.fajarca.project.moviemate.abstraction.BaseViewHolder
import io.fajarca.project.moviemate.databinding.ItemSeriesBinding
import io.fajarca.project.moviemate.databinding.ItemSeriesListBinding
import io.fajarca.project.moviemate.domain.entity.ItemClickListener
import io.fajarca.project.moviemate.domain.entity.serieslist.SeriesSection

class SeriesViewHolder(private val binding: ItemSeriesListBinding) :
    BaseViewHolder<SeriesSection>(binding) {

    private lateinit var adapter: SeriesAdapter

    companion object {
        const val LAYOUT = R.layout.item_series_list
    }

    override fun bind(item: SeriesSection, clickListener: ItemClickListener) {
        binding.tvHeader.text = item.header
        adapter = SeriesAdapter(item, clickListener)
        binding.recyclerView.layoutManager =
            LinearLayoutManager(itemView.context, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerView.adapter = adapter
        binding.recyclerView.setHasFixedSize(true)
        binding.recyclerView.isNestedScrollingEnabled = false
    }

    class SeriesAdapter(
        private val items: SeriesSection,
        private val clickListener: ItemClickListener
    ) : RecyclerView.Adapter<SeriesAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder.create(parent)
        }

        override fun getItemCount(): Int = items.series.size

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bind(items, position, clickListener)
        }

        class ViewHolder(private val binding: ItemSeriesBinding) :
            RecyclerView.ViewHolder(binding.root) {

            fun bind(movie: SeriesSection, position: Int, clickListener: ItemClickListener) {
                val item = movie.series[position]
                binding.movie = item
                binding.root.setOnClickListener { clickListener.onClick(item) }
            }

            companion object {
                fun create(viewGroup: ViewGroup): ViewHolder {
                    val layoutInflater = LayoutInflater.from(viewGroup.context)
                    val binding = ItemSeriesBinding.inflate(layoutInflater, viewGroup, false)
                    return ViewHolder(binding)
                }
            }
        }
    }
}
