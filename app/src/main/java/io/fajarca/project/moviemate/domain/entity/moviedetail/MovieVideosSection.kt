package io.fajarca.project.moviemate.domain.entity.moviedetail

import io.fajarca.project.moviemate.abstraction.BaseItemModel
import io.fajarca.project.moviemate.presentation.factory.ItemTypeFactory

data class MovieVideosSection(
    val header: String,
    val videos: List<MovieVideo>
) : BaseItemModel() {
    override fun type(itemTypeFactory: ItemTypeFactory): Int {
        return itemTypeFactory.type(this)
    }
}
