package io.fajarca.project.moviemate.abstraction

abstract class UseCase<out T, Params> {
    abstract suspend operator fun invoke(params: Params): T

    class None
}
