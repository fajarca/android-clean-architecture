package io.fajarca.project.moviemate.data.mapper.movie

import io.fajarca.project.moviemate.abstraction.Mapper
import io.fajarca.project.moviemate.data.response.movies.PopularMovieDto
import io.fajarca.project.moviemate.domain.entity.movielist.Movie
import javax.inject.Inject

class PopularMovieMapper @Inject constructor() : Mapper<PopularMovieDto, List<Movie>>() {

    override fun map(input: PopularMovieDto): List<Movie> {
        val movies = mutableListOf<Movie>()
        input.results?.forEach {
            movies.add(
                Movie(
                    it?.id ?: 0,
                    it?.title ?: "",
                    it?.originalTitle ?: "",
                    it?.overview ?: "",
                    it?.posterPath ?: "",
                    it?.backdropPath ?: "",
                    it?.voteCount ?: 0,
                    it?.voteAverage ?: 0.0f,
                    it?.popularity ?: 0.0f,
                    it?.releaseDate ?: "",
                    it?.adult ?: false
                )
            )
        }
        return movies
    }
}
