package io.fajarca.project.moviemate.domain.usecase.moviedetail

import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.domain.entity.moviedetail.Cast
import io.fajarca.project.moviemate.domain.repository.MovieRepository
import javax.inject.Inject

class GetMovieCastsUseCase @Inject constructor(private val repository: MovieRepository) {

    suspend operator fun invoke(movieId: Int): List<Cast> {
        val apiResult = repository.getMovieCasts(movieId)
        return when (apiResult) {
            is Result.Success -> apiResult.data
            is Result.Error -> emptyList()
            else -> emptyList()
        }
    }
}
