package io.fajarca.project.moviemate.data.repository

import io.fajarca.project.moviemate.data.dispatcher.CoroutineDispatcherProvider
import io.fajarca.project.moviemate.data.mapper.movie.MovieCastMapper
import io.fajarca.project.moviemate.data.mapper.movie.MovieDetailMapper
import io.fajarca.project.moviemate.data.mapper.movie.MovieImageMapper
import io.fajarca.project.moviemate.data.mapper.movie.MovieVideosMapper
import io.fajarca.project.moviemate.data.mapper.movie.NowPlayingMapper
import io.fajarca.project.moviemate.data.mapper.movie.PopularMovieMapper
import io.fajarca.project.moviemate.data.mapper.movie.SimilarMovieMapper
import io.fajarca.project.moviemate.data.mapper.movie.TopRatedMovieMapper
import io.fajarca.project.moviemate.data.source.MovieRemoteDataSource
import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.domain.entity.moviedetail.Cast
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieDetail
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieImage
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieVideo
import io.fajarca.project.moviemate.domain.entity.moviedetail.SimilarMovie
import io.fajarca.project.moviemate.domain.entity.movielist.Movie
import io.fajarca.project.moviemate.domain.repository.MovieRepository
import javax.inject.Inject

class MovieRepositoryImpl @Inject constructor(
    private val remoteDataSource: MovieRemoteDataSource,
    private val dispatcher: CoroutineDispatcherProvider,
    private val nowPlayingMapper: NowPlayingMapper,
    private val movieDetailMapper: MovieDetailMapper,
    private val popularMovieMapper: PopularMovieMapper,
    private val topRatedMovieMapper: TopRatedMovieMapper,
    private val movieImageMapper: MovieImageMapper,
    private val similarMovieMapper: SimilarMovieMapper,
    private val movieVideosMapper: MovieVideosMapper,
    private val movieCastMapper: MovieCastMapper
) : MovieRepository {
    override suspend fun getNowPlayingMovies(): Result<List<Movie>> {
        val apiResult = remoteDataSource.getNowPlayingMovies(dispatcher.io)
        return when (apiResult) {
            is Result.Success -> Result.Success(nowPlayingMapper.map(apiResult.data))
            is Result.Error -> Result.Error(apiResult.cause, apiResult.code, apiResult.errorMessage)
            else -> Result.Error()
        }
    }

    override suspend fun getMovieDetail(movieId: Int): Result<MovieDetail> {
        val apiResult = remoteDataSource.getMovieDetail(dispatcher.io, movieId)
        return when (apiResult) {
            is Result.Success -> Result.Success(movieDetailMapper.map(apiResult.data))
            is Result.Error -> Result.Error(apiResult.cause, apiResult.code, apiResult.errorMessage)
            else -> Result.Error()
        }
    }

    override suspend fun getPopularMovies(): Result<List<Movie>> {
        val apiResult = remoteDataSource.getPopularMovies(dispatcher.io)
        return when (apiResult) {
            is Result.Success -> Result.Success(popularMovieMapper.map(apiResult.data))
            is Result.Error -> Result.Error(apiResult.cause, apiResult.code, apiResult.errorMessage)
            else -> Result.Error()
        }
    }

    override suspend fun getTopRatedMovies(): Result<List<Movie>> {
        val apiResult = remoteDataSource.getTopRatedMovies(dispatcher.io)
        return when (apiResult) {
            is Result.Success -> Result.Success(topRatedMovieMapper.map(apiResult.data))
            is Result.Error -> Result.Error(apiResult.cause, apiResult.code, apiResult.errorMessage)
            else -> Result.Error()
        }
    }

    override suspend fun getMovieCasts(movieId: Int): Result<List<Cast>> {
        val apiResult = remoteDataSource.getMovieCasts(dispatcher.io, movieId)
        return when (apiResult) {
            is Result.Success -> Result.Success(movieCastMapper.map(apiResult.data))
            is Result.Error -> Result.Error(apiResult.cause, apiResult.code, apiResult.errorMessage)
            else -> Result.Error()
        }
    }

    override suspend fun getMovieImages(movieId: Int): Result<List<MovieImage>> {
        val apiResult = remoteDataSource.getMovieImages(dispatcher.io, movieId)
        return when (apiResult) {
            is Result.Success -> Result.Success(movieImageMapper.map(apiResult.data))
            is Result.Error -> Result.Error(apiResult.cause, apiResult.code, apiResult.errorMessage)
            else -> Result.Error()
        }
    }

    override suspend fun getSimilarMovies(movieId: Int): Result<List<SimilarMovie>> {
        val apiResult = remoteDataSource.getSimilarMovies(dispatcher.io, movieId)
        return when (apiResult) {
            is Result.Success -> Result.Success(similarMovieMapper.map(apiResult.data))
            is Result.Error -> Result.Error(apiResult.cause, apiResult.code, apiResult.errorMessage)
            else -> Result.Error()
        }
    }

    override suspend fun getMovieVideos(movieId: Int): Result<List<MovieVideo>> {
        val apiResult = remoteDataSource.getMovieVideos(dispatcher.io, movieId)
        return when (apiResult) {
            is Result.Success -> Result.Success(movieVideosMapper.map(apiResult.data))
            is Result.Error -> Result.Error(apiResult.cause, apiResult.code, apiResult.errorMessage)
            else -> Result.Error()
        }
    }
}
