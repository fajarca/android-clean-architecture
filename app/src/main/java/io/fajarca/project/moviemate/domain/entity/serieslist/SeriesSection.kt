package io.fajarca.project.moviemate.domain.entity.serieslist

import io.fajarca.project.moviemate.abstraction.BaseItemModel
import io.fajarca.project.moviemate.presentation.factory.ItemTypeFactory

data class SeriesSection(val header: String, val series: List<Series>) : BaseItemModel() {
    override fun type(itemTypeFactory: ItemTypeFactory): Int {
        return itemTypeFactory.type(this)
    }
}
