package io.fajarca.project.moviemate.presentation

import android.os.Bundle
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import io.fajarca.project.moviemate.R
import io.fajarca.project.moviemate.di.modules.NetworkModule
import io.fajarca.project.moviemate.dispatcher.MockWebServerDispatcher
import io.fajarca.project.moviemate.helper.launchFragmentInHiltContainer
import io.fajarca.project.moviemate.presentation.screen.movies.MoviesFragment
import io.fajarca.project.moviemate.rule.IdlingResourceRule
import io.fajarca.project.moviemate.rule.MockWebServerRule
import org.hamcrest.core.IsNot.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@UninstallModules(NetworkModule::class)
@HiltAndroidTest
@MediumTest
@RunWith(AndroidJUnit4::class)
class MoviesFragmentTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @get:Rule
    val idlingResourceRule = IdlingResourceRule()


    @get:Rule
    val mockWebServerRule = MockWebServerRule()

    @Test
    fun testRecyclerShouldVisibleWhenGetFromRemoteSuccess() {
        mockWebServerRule.server.dispatcher = MockWebServerDispatcher().SuccessDispatcher(200)

        val navController = TestNavHostController(
            ApplicationProvider.getApplicationContext()
        ).apply {
            setGraph(R.navigation.nav_main)
            setCurrentDestination(R.id.fragmentMovies)
        }

        launchFragmentInHiltContainer<MoviesFragment>(Bundle(), R.style.AppTheme) {
            Navigation.setViewNavController(view!!, navController)
        }

        onView(withId(R.id.recyclerView)).check(matches(isDisplayed()))
    }

    @Test
    fun testRecyclerShouldInvisibleWhenGetFromRemoteFailed() {
        mockWebServerRule.server.dispatcher = MockWebServerDispatcher().ErrorDispatcher(404)

        val navController = TestNavHostController(
            ApplicationProvider.getApplicationContext()
        ).apply {
            setGraph(R.navigation.nav_main)
            setCurrentDestination(R.id.fragmentMovies)
        }

        launchFragmentInHiltContainer<MoviesFragment>(Bundle(), R.style.AppTheme) {
            Navigation.setViewNavController(view!!, navController)
        }

        onView(withId(R.id.recyclerView)).check(matches(not(isDisplayed())))
    }
}
