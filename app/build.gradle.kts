plugins {
    id(BuildPlugins.androidApplication)
    id(BuildPlugins.kotlinAndroid)
    id(BuildPlugins.kotlinAndroidExtensions)
    id(BuildPlugins.kotlinKapt)
    id(BuildPlugins.navigationSafeArgs)
    id(BuildPlugins.playServices)
    id(BuildPlugins.appDistribution)
    id(BuildPlugins.detekt)
    id(BuildPlugins.hilt)
}

android {
    compileSdkVersion(AppConfiguration.compileSdk)

    buildFeatures.dataBinding = true

    defaultConfig {
        vectorDrawables.useSupportLibrary = true
        applicationId = AppConfiguration.id
        minSdkVersion(AppConfiguration.minSdk)
        targetSdkVersion(AppConfiguration.targetSdk)
        versionCode = AppConfiguration.versionCode
        versionName = AppConfiguration.versionName
        testInstrumentationRunner = "io.fajarca.project.moviemate.runner.CustomTestRunner"
    }

    buildTypes {
        getByName("debug") {
            isDebuggable = true
            buildConfigField("String", "API_BASE_URL", "\"https://api.themoviedb.org/3/\"")
            buildConfigField(
                "String",
                "API_KEY",
                "\"eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI2ZjllYjgyOTE5YjZhZmI0OGE5NWFmMGM4YzE2ZGZhYyIsInN1YiI6IjVhMTA1YTQ3YzNhMzY4NjI4OTAyZTU5ZCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.0-Y8-TepQNTOEtlE2xqOOMTlCAXOS-xQIAgybsD6i80\""
            )
            applicationIdSuffix = ".dev"
            versionNameSuffix = "-dev"

            firebaseAppDistribution {
                serviceCredentialsFile = "./app/src/main/moviemate-287307-e9246d2d4e3d.json"
                appId = "1:467567218048:android:debba962e5c65b68bfc565"
                groups = "android-team"
            }
        }

        getByName("release") {
            isDebuggable = false
            isMinifyEnabled = false
            buildConfigField("String", "API_BASE_URL", "\"https://api.themoviedb.org/3/\"")
            buildConfigField(
                "String",
                "API_KEY",
                "\"eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI2ZjllYjgyOTE5YjZhZmI0OGE5NWFmMGM4YzE2ZGZhYyIsInN1YiI6IjVhMTA1YTQ3YzNhMzY4NjI4OTAyZTU5ZCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.0-Y8-TepQNTOEtlE2xqOOMTlCAXOS-xQIAgybsD6i80\""
            )
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )

            firebaseAppDistribution {
                serviceCredentialsFile = "./app/src/main/moviemate-287307-e9246d2d4e3d.json"
                appId = "1:467567218048:android:debba962e5c65b68bfc565"
                groups = "android-team"
            }
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions { jvmTarget = "1.8" }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    // Modules
    implementation(project(Modules.stateView))
    implementation(project(Modules.shimmerview))
    implementation(project(Modules.extensions))
    implementation(project(Modules.config))
    implementation(project(Modules.resource))

    implementation(Libraries.kotlin)
    implementation(Libraries.constraintLayout)
    implementation(Libraries.recyclerview)
    implementation(Libraries.cardview)

    implementation(Libraries.appcompat)
    implementation(Libraries.material)

    // Navigation Component
    implementation(Libraries.navigation_fragment_ktx)
    implementation(Libraries.navigation_ktx)

    // Lifecycle aware component
    implementation(Libraries.lifecycleExtensions)
    kapt(Libraries.lifecycleCompiler)

    //Hilt
    implementation(Libraries.hilt)
    implementation(Libraries.hiltViewModel)
    kapt(Libraries.hiltCompiler)
    kapt(Libraries.hiltViewModelCompiler)

    // Retrofit2
    implementation(Libraries.retrofit)
    implementation(Libraries.retrofitMoshiConverter)
    implementation(Libraries.loggingInterceptor)

    //Paging
    implementation(Libraries.paging)

    // Glide
    implementation(Libraries.glide)
    kapt(Libraries.glideCompiler)

    // Timber
    implementation(Libraries.timber)

    // Idling Resource
    implementation(TestLibraries.idlingResource)

    // Firebase
    implementation(FirebaseLibraries.firebaseAnalytics)

    // Coroutine
    implementation(Libraries.coroutine)
    implementation(Libraries.coroutineAndroid)
    testImplementation(TestLibraries.coroutine)

    // Junit
    testImplementation(TestLibraries.junit)
    androidTestImplementation(TestLibraries.androidJunit)

    // Mockito
    testImplementation(TestLibraries.mockito)

    debugImplementation(TestLibraries.fragment)
    androidTestImplementation(TestLibraries.navigation)
    androidTestImplementation(TestLibraries.espresso)

    // Activity Test Rule
    androidTestImplementation(TestLibraries.activityTestRule)

    // MockWebServer
    androidTestImplementation(TestLibraries.mockWebServer)
    testImplementation(TestLibraries.mockWebServer)

    // Detekt
    detektPlugins(Libraries.detektFormatting)

    //Hilt test suite
    androidTestImplementation(TestLibraries.hiltAndroid)
    kaptAndroidTest(TestLibraries.hiltAndroidCompiler)
}
